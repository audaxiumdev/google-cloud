/**
* Description: 
*   - Constants for Google integration scripts
*/ 
define([],

function() {
    
    return {
        
        // Script Param IDs of Suitelet - Display Report 
        SCRIPT_PARAMS_SUITELET_DISPLAYREPORT : {
            REPORT_URL : 'custscript_ax_sl2_gc_displayreport_url'
        },
        
        // Script Param IDs of Portlet - Display Report 
        SCRIPT_PARAMS_PORTLET_DISPLAYREPORT : {
            PORTLET_TITLE : 'custscript_ax_pl2_gc_displayreport_title',
            PORTLET_EMBEDURL : 'custscript_ax_pl2_gc_displayreport_url',
            PORTLET_HEIGHT : 'custscript_ax_pl2_gc_displayreport_hght'
        },
        
        // Default height of the Portlet iframe
        PORTLET_DISPLAYREPORT_DEFAULT_HEIGHT : 500,

        // SCRIPT IDS
        SCRIPT_ID : {
            SS_BIGQUERY : 'customscript_ax_ss2_gc_bigquery'
        },

        LIMITS : {
            MAX_CHARACTERS  : 500000,
            MIN_UNITS       : 100,
            MAX_SECONDS     : 3000
        },
        
        // CONFIGURATION RECORD
        CONFIG_RECORD : 'customrecord_gc_configuration',
        
        // STATEMENT EMAIL QUEUE/LOG - CUSTOM RECORD
        SAVED_SEARCHES : 'customrecord_gc_saved_searches',
        
        // CONFIGURATION RECORD FIELDS
        CONFIG_RECORD_FLDS : {
            GS_SERVICE_ACCOUNT_JSON : 'custrecord_gc_service_json',
            BUCKET : 'custrecord_gc_bucket',
            PROJECT_ID : 'custrecord_gc_project_id',
            DATA_SET_ID : 'custrecord_gc_dataset_id'
        },
        
        // CONFIGURATION RECORD FIELDS
        SAVED_SEARCHES_RECORD_FLDS : {
            CONFIG_RECORD_REF : 'custrecord_gc_conf_record',
            SAVED_SEARCH : 'custrecord_gc_saved_search',
            RECORD_TYPE : 'custrecord_gc_record_type',
            LOG : 'custrecord_gc_log',
            TOTAL_ROW_COUNT : 'custrecord_gc_total_row',
            CURRENT_ROW_COUNT : 'custrecord_current_row_count',
            LAST_ID : 'custrecord_gc_last_id_pushed',
            TABLE_ID : 'custrecord_gc_sr_table_id'
        },
        
        GC_URL : {
            BIGQUERY: {
                TABLE_INSERT: 'https://www.googleapis.com/bigquery/v2/projects/projectId/datasets/datasetId/tables',
                TABLE_LIST: 'https://www.googleapis.com/bigquery/v2/projects/projectId/datasets/datasetId/tables',
                TABLEDATA_INSERTALL: 'https://www.googleapis.com/bigquery/v2/projects/projectId/datasets/datasetId/tables/tableId/insertAll'
            }
        },
    
        GC_METHOD: {
            BIGQUERY : {
                TABLE_INSERT: 'POST',
                TABLE_LIST: 'GET',
                TABLEDATA_INSERTALL: 'POST'
            }
        },

        GC_SCOPES: {
            BIGQUERY    : 'https://www.googleapis.com/auth/bigquery',
            BIGTABLE    : ''
        },
        
    };

});