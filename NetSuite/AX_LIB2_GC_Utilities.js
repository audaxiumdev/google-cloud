/**
* Description: 
*   - Utility functions for Google integration scripts
*/ 

define(['N/search', 'N/render', 'N/email', 'N/file', 'N/record', 'N/task', 'N/runtime','./AX_LIB2_GC_Constants'],

function(search, render, email, file, record, task, runtime, libConstants) {
    
    return {
        
        // Returns a map of script param ids to values
        getScriptParamValues : function(objScriptParamIds){
            
            var objValues = {};
            
            if(objScriptParamIds){
                for(var key in objScriptParamIds){
                    if(key && objScriptParamIds[key]){
                        objValues[key] = runtime.getCurrentScript().getParameter({name : objScriptParamIds[key]});
                    }
                }
            }
            
            return objValues;
        },

        //Returns the configuration record info.
        getConfigData : function(){

            var searchConfig = search.create({
                type: libConstants.CONFIG_RECORD,
                filters : [
                    search.createFilter({name: 'isinactive', operator: search.Operator.IS, values: false}),
                    search.createFilter({name: libConstants.CONFIG_RECORD_FLDS.PROJECT_ID, operator: search.Operator.ISNOTEMPTY, values: null}),
                    search.createFilter({name: libConstants.CONFIG_RECORD_FLDS.DATA_SET_ID, operator: search.Operator.ISNOTEMPTY, values: null}),
 
                ],
                columns: [
                    search.createColumn({name: libConstants.CONFIG_RECORD_FLDS.GS_SERVICE_ACCOUNT_JSON}), 
                    search.createColumn({name: libConstants.CONFIG_RECORD_FLDS.PROJECT_ID}),
                    search.createColumn({name: libConstants.CONFIG_RECORD_FLDS.DATA_SET_ID})
                ]
            });

            var searchResults = searchConfig.run().getRange({'start' : 0, 'end' : 1 });

            log.debug('getConfigData', searchResults.length);

            if (searchResults && searchResults.length > 0 ){

                return  { 
                    id          : searchResults[0].id,
                    jsonKey     : searchResults[0].getValue({name : libConstants.CONFIG_RECORD_FLDS.GS_SERVICE_ACCOUNT_JSON}),
                    projectId   : searchResults[0].getValue({name : libConstants.CONFIG_RECORD_FLDS.PROJECT_ID}),
                    datasetId   : searchResults[0].getValue({name : libConstants.CONFIG_RECORD_FLDS.DATA_SET_ID})
                };     
            
            }

            return null;
            
        },

        //Returns the searches for a given configuration record.
        getSearches : function(configRecord, arAddFilters){
            
            var arFilters = [
                search.createFilter({name: 'isinactive', operator: search.Operator.IS, values: false}),
                search.createFilter({name: libConstants.SAVED_SEARCHES_RECORD_FLDS.CONFIG_RECORD_REF, operator: search.Operator.ANYOF, values: configRecord}) 
            ];
            
            if(arAddFilters && arAddFilters.length > 0){
                arFilters = arFilters.concat(arAddFilters);
            }

            var searchConfig = search.create({
                type: libConstants.SAVED_SEARCHES,
                filters : arFilters,
                columns: [
                    search.createColumn({name: libConstants.SAVED_SEARCHES_RECORD_FLDS.SAVED_SEARCH}), 
                    search.createColumn({name: libConstants.SAVED_SEARCHES_RECORD_FLDS.RECORD_TYPE}),
                    search.createColumn({name: libConstants.SAVED_SEARCHES_RECORD_FLDS.TOTAL_ROW_COUNT}),
                    search.createColumn({name: libConstants.SAVED_SEARCHES_RECORD_FLDS.TABLE_ID}),
                    search.createColumn({name: libConstants.SAVED_SEARCHES_RECORD_FLDS.LAST_ID}),
                    search.createColumn({name: libConstants.SAVED_SEARCHES_RECORD_FLDS.CURRENT_ROW_COUNT})
                ]
            });

            var searchResults = searchConfig.run().getRange({'start' : 0, 'end' : 100 });

            log.debug('getSearches', searchResults.length);
            
            if (searchResults && searchResults.length > 0 ){

                var results = searchResults.map(function(sr){
                    return  { 
                        id              : sr.id,
                        index           : sr.getValue({name : libConstants.SAVED_SEARCHES_RECORD_FLDS.LAST_ID}),
                        currentRowCount : sr.getValue({name : libConstants.SAVED_SEARCHES_RECORD_FLDS.CURRENT_ROW_COUNT}),
                        searchid        : sr.getValue({name : libConstants.SAVED_SEARCHES_RECORD_FLDS.SAVED_SEARCH}),
                        recordType      : sr.getValue({name : libConstants.SAVED_SEARCHES_RECORD_FLDS.RECORD_TYPE}),
                        totalRows       : sr.getValue({name : libConstants.SAVED_SEARCHES_RECORD_FLDS.TOTAL_ROW_COUNT}),
                        tableId         : sr.getValue({name : libConstants.SAVED_SEARCHES_RECORD_FLDS.TABLE_ID})

                    };     

                });

                return results;

            }

            return null;
            
        },
        /**
         * Willl reschedule the script if it has ran out of time or units
         * 
         * @param  {date}       beginTime [description]
         * @param  {object}     params    script parameters
         * @return {boolean}    true if it rescheduled       
         */
        rescheduleScript : function(beginTime, params){

            var remainingUsage  = runtime.getCurrentScript().getRemainingUsage();
            var totalRunningTime = 123;
            var currentDate = new Date();

            //Total seconds between begin time
            var totalSeconds = (currentDate.getTime() - beginTime.getTime())/1000;  
            log.debug('rescheduleScript', 'USAGE: ' + remainingUsage +' | total running seconds: '+totalSeconds);


            //Reschedule if the run out of units or time
            if(remainingUsage < libConstants.LIMITS.MIN_UNITS || totalSeconds >= libConstants.LIMITS.MAX_SECONDS)
            {
                //params = {custscript_pi_delete_last_processed_rec :queueRecordID };
                log.audit('RESCHEDULING', 'USAGE: ' + remainingUsage +' | total running seconds: '+totalSeconds);

                //saveJSONfile(contents);

                var reschedule = task.create({ taskType : task.TaskType.SCHEDULED_SCRIPT });
                reschedule.scriptId = runtime.getCurrentScript().id;
                
                // Use any available adhoc deployment
                reschedule.params = params;
                
                try{

                    log.audit('checkUsage','params: '+JSON.stringify(params));
                    //log.audit('checkUsage','params.json length: ' + params.json.length );

                    reschedule.submit();

                    return true;

                } catch(ex){
                    log.error('Error during reschedule', ex.toString());
                }

            } else {
                return false;
            }




        }
    };

});