/** 
 * JWT.js
 * 
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 */

define(["N/encode",
		"N/https",
		"./jsrsasign-ns"
		], 

	function(_encode,_https, _KJUR) {

		/**
			Response:
			{
			  "access_token" : "1/8xbJqaOZXSUZbHLl5EOtu1pxz3fmmetKx9W8CV4t79M",
			  "token_type" : "Bearer",
			  "expires_in" : 3600
			}
		*/
		return {
		
			requestToken: function(stJsonkey,scope){

				//OAuth 2.0 scope information for the Fusion Tables API:
				// 			Name	Description
				// iss		The email address of the service account.
				// scope	A space-delimited list of the permissions that the application requests.
				// aud		A descriptor of the intended target of the assertion. When making an access 
				//			token request this value is always https://www.googleapis.com/oauth2/v4/token.
				// exp		The expiration time of the assertion, specified as seconds since 00:00:00 UTC, 
				// 			January 1, 1970. This value has a maximum of 1 hour after the issued time.
				// iat	The time the assertion was issued, specified as seconds since 00:00:00 UTC, January 1, 1970.

				var jsonkey;

			    // var stJsonkey 	= _runtime.getCurrentScript().getParameter({name:'custscript_ax_gc_service_account'}) || '';

			    // var scope = _runtime.getCurrentScript().getParameter({name:'custscript_ax_gc_scope'}) || '';

			    if (stJsonkey && scope){

					jsonkey = JSON.parse(stJsonkey);

			    } else {

			    	return null;
			    }
			   //  var jsonkey = 	{
						//   "type": "service_account",
						//   "project_id": "my-map-165117",
						//   "private_key_id": "7c144b3541cf8a19ef885533c99f7f5eb5ced30c",
						//   "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQC2drArgOFaIHjB\nyssSWDD3ayhOyxSTDNFdUj1b2fxJJy9sQQxqstGSNE9yocIPUy/sh+U7cKrc6dGH\no2xBxw04OQk8YHU3ly1PQOddAyMF2cTfcNlcMLjQZc63SqREp220EW4ERnVcO0YJ\nV+Gq/X1YyQ0v9Ue0q7RRu+VfznH/0fuc0Pt5+ovCMoPwgk6hLMMhdkCbnnCm0/j0\nj4zFpSQH/+N4HaUyOvlTiZ/KESPCS+OrcyIDUVcb0xkj/+7dzAO+trMrnJwgne9r\nRUWiryvUOyh/fujXOXa8rCGvlf5Zwwm2iomNnXt1YpU2Sqb/JmaVfUc10YiqpMu2\ncbUW3ti7AgMBAAECggEAAtb+Du9GkpyGxJ1fY72zpWYpjnBitZcxev5YriFKZ+kQ\nN+MbSl3WHSxrQCF8X7ivukKzMqWiQyXweOf2O1/ZuNJKboIh0OetcNpVv4DY/rJQ\n+hlLiL/JYJT1Dq0+uULR1kefKrneAb16bZLMlFi1prgaWHC82PcDK3J9M2yHIbAg\nO4KLbCPwd6dZDTQQxvSV42usK6BvuGxSRsAX5dx9OH55cAfMQxQ2HIWEcIP4C90m\nZa9ECtj5xI64NdSVm0Y8jrneJXwea7EZMxmyjR8UxBng4rqTki7v19BU0ou9j5rM\nF9n/unZ1FFDQzLIh+IXhi17/FrRP84j6JnX4TvBV4QKBgQDeBolQS+abihrg6glo\nkcJkrJEfWlZbIOcc+l33Rh+ccw6tmDCYz0qRz66H9iB9VVlbBjGdvfRSa9VcFCg9\nY3JEhZgeUSmWIO7VDRZ56Tnd30Ven9svIDBfCFWNFHJweZeB0tueCPW07fuYVmyB\n4J9eqARaziK4UfNHOHHx7oWuRwKBgQDSYmIJ4O+lBvJQkLuqEhXT6fZMmXlwqg5Q\nqkoEcV6badmirGp3FYSvnQwLRUfZquPpUIJWkGbnMF7tHLGvgWrbF2GgDxavtYO1\n7QQ3cMQj/1br2sFEEMJPO9LoNpr+eq8e0RZt6YC8zDKBA0XiMaOgNZ50gvBn+hd+\nnBYoU1r37QKBgFp9wYOHm/LzA+d9m94R4nAT4YYs1JuJuoiWBqxc2U9zmZzpNY1i\nl1r8kX76st1CZwvxZ1GKdUUOjEReNtRmbtE8vfZFUDTPMWiGUA3aH+qtMGHTt4rq\njKx/AhdhKQ9sj3KrVTWBI1vdzziUqN76hRoYUsYL1BzZ0dTKTa60b6nLAoGASD3Q\nPGQo5hoEivWn+zZKmIH2OUdaE/3Q/JH86JmlSuiF+v4JcOrKrSLYR+Z6TZwIts0W\ndHMRYsGRVwpLWWCvOJxGwZdsytkhaQ8CK6dlMpczWbm8M4strdd1DJfzkP6/s21I\nrmVH94yGqxZcoNc6wICo8Ikl1A5kku02DpLQ4eECgYANqMC/t3iAFbXCpOg8XRcX\ncXYPn2/390nf4QF/6U1BxnOKrzsAnnEpCcPVEmvamtaGGAZrNToTrhqV88j4csNY\n2fb5EbSoQqGFqo/Jz3glAt8FGXxaxfjiKF8pXIeDhi7LBtZgOzIEv9Gbbw/m912R\nMfS34RQWgaqf0iHqzEG1nQ==\n-----END PRIVATE KEY-----\n",
						//   "client_email": "fusiontable@my-map-165117.iam.gserviceaccount.com",
						//   "client_id": "107765894252175730356",
						//   "auth_uri": "https://accounts.google.com/o/oauth2/auth",
						//   "token_uri": "https://accounts.google.com/o/oauth2/token",
						//   "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
						//   "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/fusiontable%40my-map-165117.iam.gserviceaccount.com"
						// };


				var header = {
					alg:"RS256",
					typ:"JWT"
				};
				
				var claim = {
					iss 	: jsonkey.client_email,
					scope 	: scope,
					aud 	: "https://www.googleapis.com/oauth2/v4/token",
					exp 	: Math.round(Date.now()/1000)+1800, //1495194693+10800, //Math.round(Date.now()/1000) + 100000, //13.8minutes
					iat 	: Math.round(Date.now()/1000) //1494194693+10800 //Math.round(Date.now()/1000)
				};

				log.debug('CLAIM MOMENT', Date.now()/1000 |0);

				var grant_type = 'urn:ietf:params:oauth:grant-type:jwt-bearer';
				
				//<static> {String} KJUR.jws.JWS.sign(alg, spHead, spPayload, key, pass)
				var assertion = _KJUR.jws.JWS.sign('RS256', header, claim, jsonkey.private_key);

				//var assertion ='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJmdXNpb250YWJsZUBteS1tYXAtMTY1MTE3LmlhbS5nc2VydmljZWFjY291bnQuY29tIiwiaWF0IjoxNDk0MjYwODI0LCJleHAiOjE0OTQyNjIxMTMsImF1ZCI6Imh0dHBzOi8vd3d3Lmdvb2dsZWFwaXMuY29tL29hdXRoMi92NC90b2tlbiIsInN1YiI6IiIsInNjb3BlIjoiaHR0cHM6Ly93d3cuZ29vZ2xlYXBpcy5jb20vYXV0aC9mdXNpb250YWJsZXMifQ.R1siZs_HBKu65s0LrhLlw8jGKDRrWzCpihvm0ALm8ac'
				log.debug('ASSERTION', assertion);


	/*			EXAMPLE OF POST REQUEST

				POST /oauth2/v4/token HTTP/1.1
				Host: www.googleapis.com
				Content-Type: application/x-www-form-urlencoded

				grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&assertion=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiI3NjEzMjY3OTgwNjktcjVtbGpsbG4xcmQ0bHJiaGc3NWVmZ2lncDM2bTc4ajVAZGV2ZWxvcGVyLmdzZXJ2aWNlYWNjb3VudC5jb20iLCJzY29wZSI6Imh0dHBzOi8vd3d3Lmdvb2dsZWFwaXMuY29tL2F1dGgvcHJlZGljdGlvbiIsImF1ZCI6Imh0dHBzOi8vYWNjb3VudHMuZ29vZ2xlLmNvbS9vL29hdXRoMi90b2tlbiIsImV4cCI6MTMyODU3MzM4MSwiaWF0IjoxMzI4NTY5NzgxfQ.ixOUGehweEVX_UKXv5BbbwVEdcz6AYS-6uQV6fGorGKrHf3LIJnyREw9evE-gs2bmMaQI5_UbabvI4k-mQE4kBqtmSpTzxYBL1TCd7Kv5nTZoUC1CmwmWCFqT9RE6D7XSgPUh_jF1qskLa2w0rxMSjwruNKbysgRNctZPln7cqQ

	*/
				//OAuth 2.0 scope information for the Fusion Tables API:
				var url = 'https://www.googleapis.com/oauth2/v4/token';
				var headers = {};
				headers['Content-Type'] = 'application/x-www-form-urlencoded'; //application/x-www-form-urlencoded

				var body = {
					grant_type	: 'urn:ietf:params:oauth:grant-type:jwt-bearer',
					assertion 	: assertion
				};
				
				var response = _https.request({
					method  : _https.Method.POST,
					url 	: url,
					header  : headers, //,
					body 	: body
				});
				
				log.audit('requestToken RESPONSE CODE',response.code);

				log.audit('requestToken RESPONSE BODY',response.body);

				// Return the token only
				if (response.code == '200'){
					var resp = JSON.parse(response.body);
					if (resp.hasOwnProperty('access_token') ) {
						return resp.access_token;
					} 
				}
				
				return null;
			}

		};
	}
);


