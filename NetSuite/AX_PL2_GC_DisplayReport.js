/**
 * @NApiVersion 2.x
 * @NScriptType Portlet
 * @NModuleScope SameAccount
 */
 
/**
* Description: 
*   - Displays Google Data Studio Report in NS 
*
* Author: apple@audaxium.com
* Date: Feb 22, 2018
*/ 
define(['N/https',
		'N/log',
        'N/runtime',
        'N/error',
        './AX_LIB2_GC_Constants',
        './AX_LIB2_GC_Utilities'],

function(https, log, runtime, error, libConstants, libUtil){

    function render(params){
        
        log.debug('----- START -----');
        
        // Get values from script parameters
        var objParamVals;
        var objParamIds = libConstants.SCRIPT_PARAMS_PORTLET_DISPLAYREPORT;
        if(objParamIds){
            objParamVals = libUtil.getScriptParamValues(objParamIds);
        }

        // Missing params
        if(!objParamVals){
            returnErrorHtml(params, 'Required Script Params missing');
            return;
        }
   
        // No Title
        if(!objParamVals.PORTLET_TITLE){
            returnErrorHtml(params, 'Title missing');
            return;
        }
        
        // No Embed url
        if(!objParamVals.PORTLET_EMBEDURL){
            returnErrorHtml(params, 'Embed iFrame missing');
            return;
        }
        
        // Display report
        var stHeight = objParamVals.PORTLET_HEIGHT;
        if(!stHeight || parseFloat(stHeight) <= 0){
            stHeight = libConstants.PORTLET_DISPLAYREPORT_DEFAULT_HEIGHT;
        }

        var iFrameHtml = "<iframe src='" + objParamVals.PORTLET_EMBEDURL + "' marginheight='0' marginwidth='0' frameborder='0' width='100%' height='" + stHeight + "px' ></iframe>";
        
        params.portlet.title = objParamVals.PORTLET_TITLE;
        params.portlet.html = iFrameHtml;
        
        log.debug('----- END -----');
    }
    
    function returnErrorHtml(params, stMessage){
        params.portlet.title = 'Error';
        params.portlet.html = '<td><span><b>' + stMessage + '</b></span></td>';
        log.debug('----- END -----');
    }
    
    return {
        render : render
    };
    
})