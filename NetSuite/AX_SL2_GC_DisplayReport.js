/**
* @NApiVersion 2.x
* @NScriptType Suitelet
* @NModuleScope SameAccount
*/

/**
* Description: 
*   - Displays Google Data Studio Report in NS 
*
* Author: apple@audaxium.com
* Date: Feb 27, 2018
*/ 
define(['N/https',
		'N/log',
        'N/runtime',
        'N/error',
        'N/ui/serverWidget',
        './AX_LIB2_GC_Constants',
        './AX_LIB2_GC_Utilities'],

function(https, log, runtime, error, ui, libConstants, libUtil){
    
    function onRequest(context){
        
        if(context.request.method == 'GET'){
            
            log.debug('----- GET request -----');
            
            // Get URL from script parameter
            var objParamVals;
            var objParamIds = libConstants.SCRIPT_PARAMS_SUITELET_DISPLAYREPORT;
            if(objParamIds){
                objParamVals = libUtil.getScriptParamValues(objParamIds);
            }

            // Missing params
            if(!objParamVals){
                context.response.write('Required Script Params missing');
                return;
            }
            
            // No report url
            if(!objParamVals.REPORT_URL){
                context.response.write('Report URL missing');
                return;
            }
            
            // Change window location to the report URL
            var script = "<script type='text/javascript'>"
                        + " window.location = '" + objParamVals.REPORT_URL + "';"
                        + "</script>";
                        
            var form = ui.createForm({
                title: 'Opening Report...'
            });
            var objScriptFld = form.addField({
                id: 'custpage_ax_sl2_gc_dr_clientscript',
                type: ui.FieldType.INLINEHTML,
                label: 'Redirect to url'
            });
            objScriptFld.defaultValue = script;
            context.response.writePage(form);
            return;
            
            /* Commented-out: because the below logic will always show the Google Login page at the start
            // Request google report
            try{
                var objResponse = https.get({
                    url: objParamVals.REPORT_URL,
                    body: '',
                    headers: ''
                });
                
                context.response.write(objResponse.body);
                
            } catch(postError){
                log.error('GET REQUEST FAILED', postError.toString());
                context.response.write('GET REQUEST FAILED: ' + postError.toString());
            }
            */
        }
    }

    return {
        onRequest : onRequest
    }

});
