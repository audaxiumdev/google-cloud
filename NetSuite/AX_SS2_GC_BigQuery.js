/** 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define([
		"N/record",
		"N/error",
		"N/https",
		"N/runtime",
		"N/search",
		"N/file",
		"./AX_LIB_GC_TokenRequest",
		"./AX_LIB2_GC_Constants",
		"./AX_LIB2_GC_Utilities"
		],

	function(_record, _error, _https, _runtime, _search, _file, _token, _constant, _util) {

		var configRecord;
		var searches;
		var partialTransfer = false;
		var beginTime = new Date();
		var params;

		function execute(){

			log.audit(' execute','*** BEGIN ***');

			//Get the action, create a new table or insert data to a table
			var action = _runtime.getCurrentScript().getParameter({name:'custscript_ax_gc_action'}) || '';

			params = {
				custscript_ax_gc_action : action
			};

			configRecord = _util.getConfigData();

			searches = _util.getSearches(configRecord.id);

			var status = 'continue';			

			if (action == 'TABLE_INSERT'){
				
				//function to insert a new table
				searches.map(function(searchRec){
					createTable(searchRec, configRecord);
				});

			} else if (action == 'TABLEDATA_INSERTALL'){
				searches.map(function(searchRec){
					//If the script rescheduled or failed then stop
					if (status == 'continue'){
						status = insertData(searchRec, configRecord);

					}
				});
			}
			_util.rescheduleScript(beginTime,params);
			log.debug('execute','Remaining usage: '+_runtime.getCurrentScript().getRemainingUsage());
			log.debug('execute','*** END ***');

		}

		/**
		 * Method: projects.instances.tables.create
		 * POST https://bigtableadmin.googleapis.com/v2/{parent=projects/instances}/tables
		 */

		function createTable(searchRec, config){

			//var deleteGoogleTable = _runtime.getCurrentScript().getParameter({name:'custscript_aux_ss_fusion_delete'}) || false;
			//
			var clientResponse;
			// { 
			// 		jsonKey 	: '',
			// 		projectId 	: '',
			// 		datasetId 	: ''
			// }

			if (config){
			    var scope = _constant.GC_SCOPES.BIGQUERY;
				log.audit(' createTable',scope);

	 			//var bucketName = _runtime.getCurrentScript().getParameter({name:'custscript_ax_gc_bucket_name'});

				var URL = _constant.GC_URL.BIGQUERY.TABLE_INSERT;

				URL = URL.replace('projectId',configRecord.projectId);
				
				URL = URL.replace('datasetId',configRecord.datasetId);

				var body = bigQueryTable(searchRec, config);

				if (body){

					var token = _token.requestToken(config.jsonKey,scope);
					log.debug(' execute token created',token);

					if (token){

						var headers = {};
						headers['Content-Type'] = 'application/json';
						//headers['Content-Length'] = body.length; //[NUMBER_OF_BYTES_IN_FILE]
						headers['Authorization'] = 'Bearer '+token;

						log.debug('createTable ','request body: '+JSON.stringify(body));

						clientResponse = _https.post({
							url 	: URL, 
							headers : headers,
							body 	: JSON.stringify(body)
						});
						log.debug('clientResponse ',clientResponse.code);
                        log.debug('clientResponse ',clientResponse.body);

						log.debug('createTable ','END ');


					} else {

						log.error('createTable','NO TOKEN RETURNED');

					}
				} else {

					log.error('createTable','NO BODY FOR REQUEST CREATED');

				}

				
			} else {
				log.error('createTable','NO CONFIGURATION RECORD');

			}
			return clientResponse;

		}

		/**
		 * insertData Tabledata: insertAll
		 * @param  {[type]} searchRec [description]
		 * @return {[type]}           [description]
		 */
		function insertData(searchRec, config){

			log.audit('insertData searchRec',JSON.stringify(searchRec));

			var clientResponse;
			var status;
			// { 
			// 		jsonKey 	: '',
			// 		projectId 	: '',
			// 		datasetId 	: ''
			// }

			if (config){

			    var scope = _constant.GC_SCOPES.BIGQUERY;
				log.audit(' createTable',scope);

	 			//var bucketName = _runtime.getCurrentScript().getParameter({name:'custscript_ax_gc_bucket_name'});

				var URL = _constant.GC_URL.BIGQUERY.TABLEDATA_INSERTALL;

				URL = URL.replace('projectId',config.projectId);
				
				URL = URL.replace('datasetId',config.datasetId);

				URL = URL.replace('tableId',searchRec.tableId);

				var data = bigQueryTableDataInsertAll(searchRec); //{json: json, status: {index:0, lastId:0}}; 

				while (data){

					log.audit('insertData ','data: '+JSON.stringify(data.status));

					if (_util.rescheduleScript(beginTime,params) == false){

						var token = _token.requestToken(config.jsonKey,scope);

						if (token){

							log.debug('Token created',token);

							var headers = {};
							headers['Content-Type'] = 'application/json';
							//headers['Content-Length'] = body.length; //[NUMBER_OF_BYTES_IN_FILE]
							headers['Authorization'] = 'Bearer '+token;

							log.debug('insertData','BEFORE REQUEST');

							clientResponse = _https.post({
								url 	: URL, 
								headers : headers,
								body 	: JSON.stringify(data.json)
							});

							log.debug('clientResponse ',clientResponse.body);
							log.debug('clientResponse ',clientResponse.code);

							log.debug('createTable ','END ');
							
							if (clientResponse.code == '200'){

								//TODO Update the searchrecord
								//Last id pushed
								//current row count
								updateSearchRec(searchRec,data.status);
								data = bigQueryTableDataInsertAll(searchRec, data); //{json: json, status: {index:0, lastId:0}}; 
								status = 'continue';
							
							} else {
								//There was an error so stop
								data = null;
								status = 'stop';
							}

						} else {

							log.error('insertData','NO TOKEN RETURNED');
							body = null;
							status = 'stop';

						}
					} else {

						data = null;
						status = 'stop';

					}
				}

				log.audit('insertData','No more data to post');
				
			} else {
				log.error('insertData','NO CONFIGURATION RECORD');

			}
			return status;

		}



		//https://www.googleapis.com/fusiontables/v2/query?sql=INSERT%20INTO%201YyZCdi_nvEPzSqPU13CgfuYFVvS6L2A1hTv_QpTT%20(ID%2CDealer%2CAddress1%2CCity%2CCountry%2CPostalCode%2CProvince%2CPhone%2CProductsClass%2CWebsite%2CDealerType%2CPartner%2CProductName%2CIcon)%20VALUES%20(25805%2C'Ambrosia%20Natural%20Foods'%2C'55%20Doncaster%20Avenue'%2CThornhill%2CCanada%2C'L3T%201L7'%2CON%2C'(905)%20264-2510'%2C'Gorilly%20Goods'%2C'http%3A%2F%2Fwww.ambrosia.ca'%2C'HEALTH%20FOOD'%2C'-%20None%20-'%2C'Gorilly%20Goods%20-%20Baja%20Snack%20Pack'%2C'convenience')&key=AIzaSyC2yhR25IlYsd9W9o8xRaN7cFC-sNwx5X8	
		/*
		 * Executes saved search that is set in the script
		 * deployment 
		 *
		 * @param json with column/values
		 * @return {results:allResults,columns:sr.columns, index:0} 
		*/
		function getSearchResults(searchRec) {
	
			log.audit('getSearchResults',searchRec.searchid);

			var columnNames = '';

			var allResults = [];

			var resultSet;
			
			var sr = {};

			sr.columns = [];

			if (searchRec.searchid) {

				sr = _search.load({id : searchRec.searchid});

				//Add search filter
				var filters = sr.filters;
				var columns = sr.columns;	

				if (searchRec.lastId){

					filters.push(_search.createFilter({name: 'internalidnumber', operator: _search.Operator.GREATERTHAN, values: searchRec.lastId}));

				}

				//Assume more than a 1K records
				var n = 1;

				do{
					_search.create({
						type 	: _constant.SAVED_SEARCHES,
						filters : filters,
						columns : columns
					});

					resultSet = sr.run().getRange({ //TODO
						'start'	: 0,	// 'start'	: 1000 * (n-1),
						'end'	: 1000	// 'end'	: 1000 * n   	    		
					});

					log.audit('getSearchResults srResults',resultSet.length);

		    		resultSet.map(function(sr){
		    			allResults.push(sr);
		    			return true;
		    		});

		    		n++;

				} while (n<2);
			}
			


			return {results:allResults,columns:sr.columns};
		}

		/**
		 * [bigQueryTableDataInsertAll description]
		 *
		 * {
			  "kind": "bigquery#tableDataInsertAllRequest",
			  "skipInvalidRows": boolean,
			  "ignoreUnknownValues": boolean,
			  "templateSuffix": string,
			  "rows": [
			    {
			      "insertId": string,
			      "json": {
			        (key): (value)
			      }
			    }
			  ]
			}
		 * @param  {[type]} searchRec  [description]
		 * @param {[type]} [data] {json: json, status: {index:0, lastId:0}};
		 * @return {json: json, status: data.status};           [description]
		 */
		function bigQueryTableDataInsertAll(searchRec, data){
			
			log.audit('bigQueryTableDataInsertAll', 'Begin: '+searchRec.id);

			//TODO name the table
			//TODO what is the label???
			var rowData = getRows(searchRec, data); //{rows: rows, status: status}

			if (rowData){

				json =
					{
					  "kind"  				: "bigquery#tableDataInsertAllRequest",
					  "skipInvalidRows" 	: true,
					  "ignoreUnknownValues" : true,
					  "rows" 				: rowData.rows
				};

				return {json: json, status: rowData.status, index: rowData.lastId};

			} else {

				log.error('bigQueryTableDataInsertAll', 'No data from search id:'+searchRec.searchid);
				return null;

			}

			var rows = data.rows;
			var json;

			if (rows){
				json =
					{
					  "kind"  				: "bigquery#tableDataInsertAllRequest",
					  "skipInvalidRows" 	: true,
					  "ignoreUnknownValues" : true,
					  "rows" 				: rowData.rows
				};

			} else {
				log.error('bigQueryTableDataInsertAll', 'Unable to get data from search id:'+searchRec.searchid);
				return null;
			}

			if (rows){
				return {json: json, status: rowData.status};
			} else {
				return null;
			}
			
 			//return {json: json, status: rowData.status};
		}

		/**
		 * Generates the JSON to include in the post request from the 
		 * searches. 
		 * It will stop once reached the limit on data that will be sent.
		 * 
		 * @param  {object} searchRec [description]
		 * @param {object} [data] {json: json, status: {index:0, lastId:0}};
		 * @return {rows: rows, status: status}
		 */
		function getRows(searchRec, data){

			log.audit('getRows','Begin');

			var searchResults;
			var searchResultsObj;
			var index = 0;
			var status;
			var columns;

			// If there is data then this is should continue where it
			// left last time
			if (data){
				
				if (data.status.index + 1 == data.status.sr.length) {
					return null;
				}

				log.debug('getRows','continue with next batch index = '+data.status.index + ' | total rows: '+data.status.sr.length);

				searchResults = data.status.sr;
				columns = data.status.columns;
				index = data.status.index;
				status =  data.status;

			} else {
				
				log.debug('getRows','RUN SEARCH '+searchRec.searchid);

				searchResultsObj = getSearchResults(searchRec);
				searchResults = searchResultsObj.results;
				columns = searchResultsObj.columns;
				lastId = searchRec.lastId || 0;

				status = {
		    		index : 0, 
		    		lastId: lastId, 
		    		totalResults: searchResults.length
		    	};

			}

			/*
			{	"insertId": string,
				"json": {
				    (key): (value)
				}
			}
			*/

			var oneRow = {
		      "insertId": 1,
		      "json": {}
		    };
		    var value;

		    var size = 0;

		    var rows = [];

		    var i; 

		    for (i = index; i < searchResults.length; i++) {

		    	sr = searchResults[i];

    			oneRow = {};
				oneRow.insertId = sr.id;
				oneRow.json = {};

				//Add all columns in the search
				columns.map(function(col){

					colDefinition = col.label.split('_');
					value = sr.getValue(col);

					if (value){

						oneRow.json[colDefinition[1]] = value;

					} else {

						oneRow.json[colDefinition[1]] = null;
					
					}
				});

				size += JSON.stringify(oneRow).length;

				if (size >= _constant.LIMITS.MAX_CHARACTERS) {

					// updateSearchRec(searchRec,i);
					// partialTransfer = true;
					log.audit('getRows','Max number of characters reached '+size +' | lastId = '+sr.id);
					status.lastId 	= sr.id;
					break;
				
				} else {
					status.index = i;
					status.lastId = sr.id;
					rows.push(oneRow);
				}

		    }

		    if (size > 0){
		    	//status.index  	= i; 
		    	//status.lastId 	= searchResults[i].id;
		    	status.numRows 	= searchResults.length;
		    	status.sr 		= searchResults;
		    	status.columns 	= columns;

		    	return {rows: rows, status: status};

		    } else {
		    	return null;
		    }
		    

		}

		function bigQueryTable(searchRec, configRecord){

			//TODO name the table
			//TODO what is the label???
			var tableId = searchRec.tableId;
			var schema = getSchema(searchRec.searchid);
			var json;

			if (schema){
				json =
					{
					  "kind": "bigquery#table",
					  "tableReference": {
					    "projectId": configRecord.projectId,
					    "datasetId": configRecord.dataSetId,
					    "tableId": tableId
					  },
					  "friendlyName": tableId,
					  "description": 'some description',
					  "labels": {
					    key: 'test'
					  },
					  "schema": {
					    "fields": schema
					  }	  
				};

			} else {
				log.error('convertBigQuery', 'Unable to create schema');
			}
			
 			
 			return json;
		}

		function getSchema(searchid){
		 // [
		 //    {
		 //        "name": string,
		 //        "type": string,
		 //        "mode": string,
		 //        "fields": [
		 //          (TableFieldSchema)
		 //        ],
		 //        "description": string
		 //    }
		 // ]
		    
		    var schema = [];

		    var col = {};

		 	var savedSearch = _search.load({
		 		id : searchid
		 	});

		 	var columns = savedSearch.columns;

		 	for (var i = 0; i < columns.length; i++) {

		 		colInfo = columns[i].label.split('_');
				col = {};
				col.name = colInfo[1];
				col.type = colInfo[0];
				col.mode = 'NULLABLE';
				col.fields = null; //nested fields
				col.description = colInfo[1];

				if (col.name == 'ID'){
					col.mode = 'REQUIRED';
				} else {
					col.mode = 'NULLABLE';
				}
				
				schema.push(col);

		 	}
		 	
		 	return schema;
		}
		
		/**
		 * [updateSearchRec description]
		 * @param  {[type]} 						searchRec [description]
		 * @param  {status: {index:0, lastId:0}}	status     [description]
		 * @return {[type]}
		 */
		function updateSearchRec(searchRec,status){

			var srRec = _record.load({
				id : searchRec.id,
				type : _constant.SAVED_SEARCHES
			});

			srRec.setValue({
				fieldId: 'custrecord_gc_total_row',
				value: status.numRows
			});

			srRec.setValue({
				fieldId: 'custrecord_gc_last_id_pushed',
				value: status.lastId
			});

			srRec.setValue({
				fieldId: 'custrecord_current_row_count',
				value: status.index
			});

			try{

				srRec.save();

			} catch(err){

				log.error('updateSearchRec', err.toString());
			}	
			

		}

	
	return {
		execute: execute
	};
});
