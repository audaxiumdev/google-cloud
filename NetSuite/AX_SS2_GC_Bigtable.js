/** 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define([
		"N/record",
		"N/error",
		"N/https",
		"N/runtime",
		"N/search",
		"./google_token_request_2"
		],

	/**
	 * 
     * Updates fusion table in Google
     * 
	 * Response from Google
	 * {
	 *  "kind": "fusiontables#task",
	 *  "taskId": long,
	 *  "started": boolean,
	 *  "progress": string,
	 *  "type": string
	 * }
	 * {
	 *  "kind": "fusiontables#sqlresponse",
	 *    "columns": [
	 *    "Dealer",
	 *    "Country"
	 *   ]
	 * }
	 * Get all columns
	 * https://www.googleapis.com/fusiontables/v2/tables/tableId/columns
	 *
	 * { "error": { "errors": [ { "domain": "usageLimits", "reason": "keyInvalid", "message": "Bad Request" } ], "code": 400, "message": "Bad Request" } }	
	 * @module 
	 * @requires N/record
	 * @requires N/error
	 *
	 * @returns {object} 
	 */
	function(_record, _error, _https, _runtime, _search, _token) {

		var tableId;
		var fusionTableAPI;	
		var requestURL;
		var importURL;
		var columnNames;
		var arrColumnNames;

		function execute(){

			log.audit(' execute','BEGIN');

			//getGoogleData();

			//var deleteGoogleTable = _runtime.getCurrentScript().getParameter({name:'custscript_aux_ss_fusion_delete'}) || false;
		    var stJsonkey 	= _runtime.getCurrentScript().getParameter({name:'custscript_ax_gc_bt_serviceaccount'}) || '';

		    var scope = _runtime.getCurrentScript().getParameter({name:'custscript_ax_gc_bt_scope'}) || '';
			log.audit(' execute',scope);

			// Request token
			var token = _token.requestToken(stJsonkey,scope);
			//var token ="ya29.c.ElpZBSIZI0o0YaPBWijUvplPRgiYQmYnJtXNGOkwA7HAXGeCEaKxLHmnzTdiiZdQNWLJ09n9Mx3ZGDlzmn5JAt0TQ3I1hyCnFp3K0IISQsZ2t4oc00n3-On38qs";

			var clientResponse;

			//var token = 'ya29.c.EloVBQLTlT3ZpS9kfNzCfy5FuI0W_XVlbgg30P9ZFEuPR8D8hVSw3bKoOFsJinsjL7Sd3qg9BlTvAVZShUz_P2o9S67zA1Hsj4ul8E1473we5t1dvKVltcNCB7A';
			
			log.debug(' execute token created',token);

			if (token) {

				var allRows = searchRecords();
				
				clientResponse = createTable(allRows,token);

				log.debug('INSERT response code',clientResponse.code);
				log.debug('response HEADERS',clientResponse.headers);
				log.debug('response BODY',clientResponse.body);
				

			}

			log.debug('execute','END');


		}

		/**
		 * Method: projects.instances.tables.create
		 * POST https://bigtableadmin.googleapis.com/v2/{parent=projects/instances}/tables
		 */

		function createTable(allRows,token){

 			//POST https://bigtableadmin.googleapis.com/v2/{parent=projects/*/instances/*}/tables
 			//projects/<project>/instances/<instance>.
 			//
 			// SCOPE https://www.googleapis.com/auth/bigtable.admin.table

 			
 			log.debug('createTable','BEGIN');

 			//var bucketName = _runtime.getCurrentScript().getParameter({name:'custscript_ax_gc_bucket_name'});

			var URL = "https://bigtableadmin.googleapis.com/v2/projects/g-cloud-76029/instances/audaxium-test/tables";
			//var URL = 'https://bigtableadmin.googleapis.com/v2/parent=projects/g-cloud-76029/instances/audaxium-test/tables';
			//var URL = 'https://bigtableadmin.googleapis.com/v2/\{parent=projects/g-cloud-76029/instances/audaxium-test\}/tables';

			//colNames = 'ID,Dealer,Address1,City,Country,PostalCode,Province,Phone,ProductsClass,Website,DealerType,Partner,ProductName,Icon';

			//var body = JSON.stringify(allRows.arrValues);
			var body = convertBigTable(allRows.arrValues);

			var headers = {};
			headers['Content-Type'] = 'application/json';
			//headers['Content-Length'] = body.length; //[NUMBER_OF_BYTES_IN_FILE]
			headers['Authorization'] = 'Bearer '+token;

			log.debug('importRows ','response body: '+JSON.stringify(body));

			var clientResponse = _https.post({
				url 	: URL, 
				headers : headers,
				body 	: JSON.stringify(body)
			});

			log.debug('createTable ','END ');


			return clientResponse;

		}

		function searchRecords(){

			// Search the records
			var srResultsObj 	= getSearchResults();
			var srResults 		= srResultsObj.results;
			var columns 		= srResultsObj.columns;

			log.debug('execute srResults',srResults.length);
							
			var getTextColumnNames = ["ProductClass","Country","Partner"];
			var columnNames = '';

			columns.map(function(){
				columnNames += columns[i].label + ',';
			});
			//Add the values into an array
			arrColumnNames = columns.map(function(){
				return columns[i].label;
			});
			
			columnNames.substring(0,columnNames.length-1);

			var values = srResults.map(function(searchResult){
				row = '';
				for (var i = 0; i < columns.length; i++) {

					fieldValue = "";
					if (getTextColumnNames.indexOf(columns[i].label) == -1)
						fieldValue = searchResult.getValue(columns[i]);
					else
						fieldValue = searchResult.getText(columns[i]);

					fieldValue = fieldValue ? fieldValue : '';
					
					row += fieldValue+",";

				}

				return row.substring(0,row.length-1);
			});

			//Do it again JSON array for the CSV

			var arrValues = srResults.map(function(searchResult){

				var row = {};

				for (var i = 0; i < columns.length; i++) {

					if (getTextColumnNames.indexOf(columns[i].label) == -1) {

						value = searchResult.getValue(columns[i]);
							// columns[i].label : 
						//row.push(searchResult.getValue(columns[i]));
					}
					else {

						value = searchResult.getText(columns[i]);
						//row.push(searchResult.getText(columns[i]));
					}

					row[columns[i].label] = value;
		
				}

				return row;
			});

			return {
					columnNames : columnNames,
					values 		: values, //arr of rows comma separated values
					//arrCols 	: columnNames,
					arrValues	: arrValues
				};


		}

		function getGoogleData(){

			//Get the script parameters
			
			tableId 		= _runtime.getCurrentScript().getParameter({name:'custscript_aux_ss_fusion_table_id'}) || '';//1YyZCdi_nvEPzSqPU13CgfuYFVvS6L2A1hTv_QpTT
		    fusionTableAPI 	= _runtime.getCurrentScript().getParameter({name:'custscript_aux_fusion_api_key'}) || '';
		    //importURL = 'https://www.googleapis.com/upload/fusiontables/v2/tables/'+tableId+'/import';

		    //AIzaSyC2yhR25IlYsd9W9o8xRaN7cFC-sNwx5X8
		    //AIzaSyCEpWcL2oGkwEqp3_TPohSc068eLaQ4vZU
		    fusionTableAPI = 'AIzaSyA4nSadRxPR-41ynnwawb2pwYZ3q6ltRl4';

			requestURL = "https://www.googleapis.com/fusiontables/v2/query";

			log.debug('tableId',tableId);

			log.debug('fusionTableAPI',fusionTableAPI);


		}

		//https://www.googleapis.com/fusiontables/v2/query?sql=INSERT%20INTO%201YyZCdi_nvEPzSqPU13CgfuYFVvS6L2A1hTv_QpTT%20(ID%2CDealer%2CAddress1%2CCity%2CCountry%2CPostalCode%2CProvince%2CPhone%2CProductsClass%2CWebsite%2CDealerType%2CPartner%2CProductName%2CIcon)%20VALUES%20(25805%2C'Ambrosia%20Natural%20Foods'%2C'55%20Doncaster%20Avenue'%2CThornhill%2CCanada%2C'L3T%201L7'%2CON%2C'(905)%20264-2510'%2C'Gorilly%20Goods'%2C'http%3A%2F%2Fwww.ambrosia.ca'%2C'HEALTH%20FOOD'%2C'-%20None%20-'%2C'Gorilly%20Goods%20-%20Baja%20Snack%20Pack'%2C'convenience')&key=AIzaSyC2yhR25IlYsd9W9o8xRaN7cFC-sNwx5X8	
		/*
		 * Executes saved search that is set in the script
		 * deployment 
		 *
		 * @param json with column/values
		 * @return nlobjSearchResults 
		*/
		function getSearchResults() {

			var searchid = _runtime.getCurrentScript().getParameter({name:'custscript_ax_ns_search'});
			
			var columnNames = '';

			var allResults = [];

			var resultSet;
			
			var sr = {};

			sr.columns = [];

			if (searchid) {

				sr = _search.load({id : searchid});				

				//Assume more than a 1K records
				var n = 1;

				do{
					
					resultSet = sr.run().getRange({
						'start'	: 0,	// 'start'	: 1000 * (n-1),
						'end'	: 1000	// 'end'	: 1000 * n   	    		
					});
					log.debug('getSearchResults srResults',resultSet.length);

			    		resultSet.map(function(sr){
			    			allResults.push(sr);
			    			return true;
			    		});
			    		n++;

				} while (n<2);
			}
			


			return  {results:allResults,columns:sr.columns};
		}


		function convertBigTable(data){

			/*
				{
				  "tableId": string,
				  "table": {
				    object(Table)
				  },
				  "initialSplits": [ /-> optional
				    {
				      object(Split)
				    }
				  ],
				}
			SCOPE
 			https://www.googleapis.com/auth/bigtable.admin.table

			{
			  "name": table name,
			  "columnFamilies": {
			    string: {
			      object(ColumnFamily)
			    },
			    ...
			  },
			  "granularity": enum(TimestampGranularity),
			}
 			
 			 */
 			
 			 var table = {
 			 	//name 			: 'customers',
 			 	columnFamilies  : {},
 			 	granularity: 'MILLIS'
 			 };

 			var columnFamilies =   {
		        "family_int" : {
		          "familyId": "family_int",
		          "type": "INTEGER",
		          "encoding": "BINARY",
		          "onlyReadLatest": "true",
		          "columns": [
		            {
		              "qualifierString": "foo",
		              "onlyReadLatest": "true",
		            }
		          ]
		        }
		    };
   

 			 //var columnFamilies = {hello :{ "name": "wrench", "mass": "1.3kg", "count": "3" }};
 			 // var columnFamilies = {
 			 // 	"ID": "1542","Dealer":"Aaron Abbott","Address1":"1500 Broadway","City":"Denver","Country":"United States","PostalCode":"80202","Province":"CO","ProductClass":null,"Website":"-none-","DealerType":"BIKE","Partner":null,"ProductName":"Product","Icon":"star","Lat":"39.7404939","Lng":"-104.9874131"
 			 // }; 

 			 table.columnFamilies = columnFamilies;

 			 var json = {
				  "tableId" 		: 'customers',
				  "table" 			: table,
				 //  "initialSplits" 	:{
  			// 			"key": string,
					// }
				};

 			 return json;
		}




		/**
		 * Turns a Array of json and converts it to a CSV
		 * WITHOUT TITLES
		 * @param jsonArray - [{name:value, name2: value2},... ]
		 * 
		 * @return string
		 *
		 */
		 function JSONtoCSV(jsonArray) {

		 	var csv = '';

		 	//This does not need the titles
		 	//Add the CSV titles
		 	// for (var element in jsonArray[0]) {
		 	// 	csv += '"'+element+'",'
		 	// }

		 	//Eliminate the last comma
		 	// csv = csv.substring(0,csv.length-1);
		 	// csv += '\n';

		 	//each element of the array is a row in the csv
		 	jsonArray.map(function(row){
		 		for (var element in row) {
		 			if (typeof(row[element]) != 'undefined'){
		 				csv += '"'+row[element]+'",';
		 			}
		 		}
		 		//Eliminate the comma
		 		csv = csv.substring(0,csv.length-1);
		 		csv += '\n';
		 	});

		 	log.debug('CSV output',csv);
		 	
		 	return csv;
		 }
	
	/*	

		var requestParameters = {
				tableId 	: '',	// string	Table whose rows will be replaced.
				uploadType 	: '',	//string REQUIRED	The type of upload request to the /upload URI. Acceptable values are:
				//media - Simple upload. Upload the media only, without any metadata.
				//multipart - Multipart upload. Upload both the media and its metadata, in a single request.
				//resumable - Resumable upload. Upload the file in a resumable fashion, using a series of at least two requests where the first request includes the metadata.
				//Optional query parameters
				delimiter 	: '', //	string	The delimiter used to separate cell values. This can only consist of a single character. Default is ,.
				encoding 	: '', //string	The encoding of the content. Default is UTF-8. Use 'auto-detect' if you are unsure of the encoding.
				endLine 	: '', //	integer	The index of the line up to which data will be imported. Default is to import the entire file. If endLine is negative, it is an offset from the end of the file; the imported content will exclude the last endLine lines.
				isStrict 	: '', //	boolean	Whether the imported CSV must have the same number of column values for each row. If true, throws an exception if the CSV does not have the same number of columns. If false, rows with fewer column values will be padded with empty values. Default is true.
				startLine 	: '' //	integer	The index of the first line from which to start importing, inclusive. Default is 0.
			}
					Operation	Description	Query format of the "sql" parameter
		list	Lists all rows within a table.	GET with a specific table ID: 
		  SELECT ROWID FROM <table_id>
		get	Gets a specific row.	GET with a specific table ID and criteria:
		  SELECT ROWID FROM <table_id> WHERE <your filter>
		insert	Inserts a new row into a table.	POST with a specific ROWID, where you pass in data for a new row: 
		  INSERT INTO <table_id> (<column_name> {, <column_name>}*) 
		    VALUES (<value> {, <value>}*)
		update	Updates a specific row.	POST with a specific ROWID, where you pass in data for the updated row: 
		  UPDATE <table_id> SET <column_name> = <value> {, <column_name> = <value> }* 
		    WHERE ROWID = <row_id>
		delete	Deletes a specific row.	POST with a specific ROWID: 
		  DELETE FROM <table_id> {WHERE ROWID = <row_id>}
		
		{ "error": { "errors": [ { "domain": "fusiontables", "reason": "cannotWriteDataOnGetRequests", "message": "The operation is not supported for GET requests. Please try again using POST." } ], "code": 501, "message": "The operation is not supported for GET requests. Please try again using POST." } }	
		{ "error": { "errors": [ { "domain": "usageLimits", "reason": "keyInvalid", "message": "Bad Request" } ], "code": 400, "message": "Bad Request" } }
		{ "error": { "errors": [ { "domain": "global", "reason": "required", "message": "Login Required", "locationType": "header", "location": "Authorization" } ], "code": 401, "message": "Login Required" } }	
		
*/
	return {
		execute: execute
	};
});
